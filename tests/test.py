import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import json
from time import sleep

URL = 'http://localhost:3000'


class ButterflyTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get(URL + '/resetFactory')
        print('setting up')

    def run_test(self, tt):
        driver = self.driver
        phases = tt if type(tt) == list else [tt]
        for t in phases:
            if "url" in t:
                driver.get(URL + t["url"])
            # sleep(2)
            if "click" in t:
                if "class" in t["click"]:
                    elem = driver.find_element_by_css_selector(".%s:nth-child(%s)"%(t["click"]["class"], t["click"]["index"] or "0"))
                elif "id" in t["click"]:
                    elem = driver.find_element_by_id(t["click"]["id"])
                elif "name" in t["click"]:
                    elem = driver.find_element_by_name(t["click"]["name"])
                print(elem)
                if elem:
                    elem.click()
                else:
                    print("NO CLICKABLE FOUND")
            if "form" in t:
                for field, value in t["form"]["fields"].items():
                    elem = driver.find_element_by_name(field)
                    elem.send_keys(value)
                if "submitId" in t["form"].items():
                    elem = driver.find_element_by_id(t["form"]["submitId"])
                    elem.click()
                else:
                    elem.send_keys(Keys.RETURN)
    
            for e in t.get("expectations", []):
                source = driver.page_source
                # print(source)
                selector_type = e.get("selector_type", None)

                if selector_type == "id":
                    source = driver.find_element_by_id(e["elem"])
                elif selector_type == "name":
                    source = driver.find_element_by_name(e["elem"])
                elif selector_type == "class":
                    source = driver.find_element_by_css_selector(".%s:nth-child(%s)"%(e["elem"],e.get("index", "0")))
                elif selector_type == "xpath":
                    source = driver.find_element_by_xpath(e["elem"])
                elif selector_type == "css":
                    source = driver.find_element_by_css_selector(e["elem"])
                # print(e.items())
                attr = e.get("attr", "innerHTML")
                if type(source) != str:
                    source = source.get_attribute(attr)
                # print("SOURCE:", source)
                if e["op"] == "contains":
                    assert e["pattern"] in source
                elif e["op"] == "not contains":
                    assert e["pattern"] not in source
                elif e["op"] == "equals":
                    assert e["pattern"] == source
                elif e["op"] == "not equals":
                    assert e["pattern"] != source

    def tearDown(self):
        self.driver.close()

    def task_logout(self):
        self.run_test([
            {
                "url": "/panel",
                "expectations": [
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"ثبت نام",
                        "op": "not contains"
                    },
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"پنل باترفلای",
                        "op": "contains"
                    },
                ]
            },
            {
                "url": "/user/signout",
                "expectations": [
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"ورود به پنل",
                        "op": "contains"
                    },
                ]
            }
        ])

    def task_login(self, phone):
        self.run_test([
            {
                "url": "/user/signin",
                "form": {
                    "fields":{
                        "phone": phone
                    }
                },
                "expectations": [{
                    "selector_type": "id",
                    "elem": "body",
                    "pattern":"احراز هویت",
                    "op": "contains"
                }]
            },
            {
                "form": {
                    "fields": {
                        "token": "1234",
                    },
                },
                "expectations": [
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"ثبت نام",
                        "op": "not contains"
                    },
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"پنل باترفلای",
                        "op": "contains"
                    },
                ]
            }
        ])


    def test_all_things(self):
        print('test signup')
        self.run_test([
            {
                "url": "/user/signup",
                "form": {
                    "fields":{
                        "firstName": "Hamed",
                        "lastName": "Valizadeh",
                        "iban": "IR190540106670000484591006",
                        "natCode": "0923147993",
                        "email": "havaliza@gmail.com",
                        "phone": "09369923074"
                    }
                },
                "expectations": [{
                    "selector_type": "id",
                    "elem": "body",
                    "pattern":"احراز هویت",
                    "op": "contains"
                }]
            },
            {
                "form": {
                    "fields": {
                        "token": "1234",
                    },
                },
                "expectations": [
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"ثبت نام",
                        "op": "not contains"
                    },
                    {
                        "selector_type": "id",
                        "elem": "body",
                        "pattern":"پنل باترفلای",
                        "op": "contains"
                    },
                ]
            }
        ])
        print('test logout')
        self.task_logout()
        print('test login')
        self.task_login('09369923074')
        print('test charge')
        self.run_test([
            {
                "url": "/transaction/charge",
                "form": {
                    "fields":{
                        "amount": "10000",
                        "currency": "IRR",
                    }
                },
                "expectations": [{
                    "selector_type": "id",
                    "elem": "body",
                    "pattern":"موفق",
                    "op": "contains"
                }]
            }
        ])
        print('test transfer')
        self.run_test([
            {
                "url": "/transaction/transfer",
                "form": {
                    "fields":{
                        "amount": "1000",
                        "currency": "IRR",
                        "destination": "09215445325",
                    }
                },
                "expectations": [{
                    "selector_type": "id",
                    "elem": "body",
                    "pattern":"موفق",
                    "op": "contains"
                }]
            }
        ])
        print('test exchange')
        self.run_test([
            {
                "url": "/transaction/exchange",
                "form": {
                    "fields":{
                        "amount": "1000",
                        "source_currency": "IRR",
                        "dest_currency": "USD",
                    }
                },
                "expectations": [{
                    "selector_type": "id",
                    "elem": "body",
                    "pattern":"موفق",
                    "op": "contains"
                }]
            }
        ])
        print('test service creation')
        self.task_logout()
        self.task_login('09215445325')
        self.run_test([
            {
                "url": "/service/create",
                "form": {
                    "fields":{
                        "description": "خرید گل آنلاین",
                        "name": "گل سرویس",
                        "price": "1",
                        "currency": "USD",
                    }
                },
                "expectations": [{
                    "selector_type": "id",
                    "elem": "body",
                    "pattern":"موفق",
                    "op": "contains"
                }]
            }
        ])
      


    # def test_rquest_create_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/request/create/5b278f40f18fc2fbbdd6514a",

    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_rquest_reject_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/request/reject/5b27b5e5d0f47a1ab86b033d",

    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_rquest_approve_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/request/approve/5b27b5e5d0f47a1ab86b033d",

    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

        
            
    # def test_signup_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signup",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "lastName": "abedi",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com",
    #                 "phone": ""
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_signup_first_name(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signup",
    #         "form": {
    #             "fields":{
    #                 "lastName": "abedi",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
        
    # def test_signup_last_name(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signup",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_signup_iban(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signup",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "lastName": "abedi",
    #                 "iban": "1234569012378901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_signup_nat(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signup",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "lastName": "abedi",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_contact_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/contact",
    #         "form": {
    #             "fields":{
    #                 "name": "aminabedi",
    #                 "body":'hey there',
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)


    # def test_contact_name(self):
    #     t = {
    #         "url": "http://localhost:2345/contact",
    #         "form": {
    #             "fields":{
    #                 "message":'hey there',
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "alert",
    #             "attr": "innerHTML",
    #             "pattern":"نادرست",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_contact_email(self):
    #     t = {
    #         "url": "http://localhost:2345/contact",
    #         "form": {
    #             "fields":{
    #                 "body":'hey there',
    #                 "name": "aminabedi"
                    
    #             }
    #         },
    #         "expectations": [{
    #                "selector_type": "class",
    #             "elem": "alert",
    #             "attr": "innerHTML",
    #             "pattern":"نادرست",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_contact_message(self):
    #     t = {
    #         "url": "http://localhost:2345/contact",
    #         "form": {
    #             "fields":{
    #                 "name": "aminabedi",
    #                 "email": "aminabedi96@gmail.com"
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "alert",
    #             "attr": "innerHTML",
    #             "pattern":"نادرست",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_signin_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signin",
    #         "form": {
    #             "fields":{
    #                 "phone": "09215445325",
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "elem": "phone",
    #             "attr": "value",
    #             "pattern":"09",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_verify_code(self):
    #     t = {
    #         "url": "http://localhost:2345/user/verify?phone=09215445325",
    #         "form": {
    #             "fields":{
    #                 "token": "1235",
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"نادرست",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_verify_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/verify?phone=09215445325",
    #         "form": {
    #             "fields":{
    #                 "token": "1234",
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "fi-alert",
    #             "attr": "innerHTML",
    #             "pattern":"اشتباه",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_charge_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/transaction/charge",
    #         "form": {
    #             "fields":{
    #                 "amount": 120
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "fi-alert",
    #             "attr": "innerHTML",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_service_create_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/service/create",
    #         "form": {
    #             "fields":{
    #                 "name": "testService",
    #                 "description": "test of a service creation",
    #                 "price": 10,
    #                 "currency": "EUR"
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_exchange_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/transaction/exchange",
    #         "form": {
    #             "fields":{
    #                 "amount": 100,
    #                 "source_currency": "IRR",
    #                 "dest_currency": "EUR"
    #             }

    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_messages_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/contact/list",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"پیام",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_rates_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/rates",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"دلار",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_users_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/list",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"نام",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_signout_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/signout",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"دلار",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_profile_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"نام",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_profile_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "lastName": "abedi",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com",
    #                 "phone": ""
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_profile_first_name(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "lastName": "abedi",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
        
    # def test_profile_last_name(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_profile_iban(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "lastName": "abedi",
    #                 "iban": "1234569012378901234",
    #                 "nationalCode": "0019145251",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_profile_nat(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "firstName": "amin",
    #                 "lastName": "abedi",
    #                 "iban": "IR123456789012345678901234",
    #                 "nationalCode": "0019145",
    #                 "email": "aminabedi96@gmail.com"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "class",
    #             "elem": "form-error",
    #             "attr": "innerHTML",
    #             "pattern":"اجباری",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_transfor_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/transaction/transfer",
    #         "form": {
    #             "fields":{
    #                 "amount": 1,
    #                 "currency": "IRR",
    #                 "destination": "09369923074"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"نادرست",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_transfor_insufficient(self):
    #     t = {
    #         "url": "http://localhost:2345/transaction/transfer",
    #         "form": {
    #             "fields":{
    #                 "amount": 100000000000,
    #                 "currency": "IRR",
    #                 "destination": "09369923074"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"کافی",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_transactions_list(self):
    #     t = {
    #         "url": "http://localhost:2345/transaction/list",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"مبدا",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)

    # def test_landing(self):
    #     t = {
    #         "url": "http://localhost:2345/",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"سامانه خدمات ارزی",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_landing(self):
    #     t = {
    #         "url": "http://localhost:2345/tos",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"قوانین",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_exchange_lab(self):
    #     t = {
    #         "url": "http://localhost:2345/rates/lab",
    #         "form": {
    #             "fields":{
    #                 "amount": 1,
    #                 "from_currency": "USD",
    #                 "to_currency": "IRR"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"42000",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_role_assignment(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "role":"Employee"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_role_assignment(self):
    #     t = {
    #         "url": "http://localhost:2345/user/profile",
    #         "form": {
    #             "fields":{
    #                 "active":"false"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_role_assignment(self):
    #     t = {
    #         "url": "http://localhost:2345/system/profile",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"موجودی",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_role_assignment(self):
    #     t = {
    #         "url": "http://localhost:2345/system/charge",
    #         "form": {
    #             "fields":{
    #                 "amount": 120
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"موجودی",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_messages_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/contact/list",
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"balance",
    #             "op": "contains"
    #         }]
    #     }
    #     self.run_test(t)
    # def test_messages_ok(self):
    #     t = {
    #         "url": "http://localhost:2345/system/criteria",
    #         "form": {
    #             "fields":{
    #                 "lower_bound": "100",
    #                 "upper_bound": "100000000"
    #             }
    #         },
    #         "expectations": [{
    #             "selector_type": "all",
    #             "pattern":"خطا",
    #             "op": "not contains"
    #         }]
    #     }
    #     self.run_test(t)
    

if __name__ == "__main__":
    unittest.main()



# driver = webdriver.Chrome()
# driver.get("http://blog.ir")
# assert "بلاگ" in driver.title
# elem = driver.find_element_by_name("q")
# elem.clear()
# elem.send_keys("pycon")

# assert "No results found." not in driver.page_source
