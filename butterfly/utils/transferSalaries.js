const mongoose = require('mongoose')
const User = mongoose.model('User')
const Transaction = mongoose.model('Transaction')
const Promise = require('bluebird')

module.exports = exports = async ()=>{
    const admin = await User.findOne({admin: true})
    const emps = await User.find({role: "Employee"})
    console.log("SALARIES:", emps)
    Promise.map(emps, async(emp)=>{
        if(emp.salary>0){
            return await new Transaction({
                made_by: admin,
                source: admin,
                destination: emp,
                amount: emp.salary,
                currency: "IRR",
                action: "salary"
            }).save()
        }
    })
}