const mongoose = require('mongoose')
const Request = mongoose.model('Request')
const User = mongoose.model('User')
const treshold = 24 * 60 * 60 * 1000
const Promise = require('bluebird')
module.exports = exports = async ()=>{
    const admin =await User.findOne({admin:true})
    const requests = await Request.find({state: "PENDING", created_on: {$lt: Date.now() - 60*1000}})

    await Promise.map(requests, async (req)=>{
        req.state = "TIMEOUT REJECT"
        return await req.save()
    })
}