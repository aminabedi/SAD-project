const axios = require("axios");

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, "g"), replace);
}

const getRials = (data, field) =>
  parseInt(replaceAll(data[field]["قیمت"], ",", ""), 10);

const getRates = async () => {
  const { data } = await axios.get("https://www.faranevis.com/api/currency/");
  const USD = getRials(data, "دلار");
  const EUR = getRials(data, "یورو");
  return { USD, EUR };
};

module.exports = getRates;
