var mongoose = require('mongoose');
var Schema  = mongoose.Schema;
var schema = new Schema({
    user: {type: mongoose.Schema.ObjectId,required: true, ref: 'User'},
    service: {type: mongoose.Schema.ObjectId,required: true,ref: 'Service'},
    reviewed_by: {type: mongoose.Schema.ObjectId, ref: 'User'},
    type: {type: String, enum: ["Withdraw", "Service"], required: true},
    amount: {type: Number, default: 0},
    state: {type: String, enum: ["PENDING", "APPROVED", "REJECTED", "TIMEOUT REJECT"], default: "PENDING"},
    created_on: {type: Date, default: Date.now}

},  {toObject: {virtuals: true}, toJSON: {virtuals: true}});

schema.post('save', async function(){
    const Service = mongoose.model('Service'), User = mongoose.model('User'), Message = mongoose.model('Message')
    let service = await Service.findById(this.service), user = await User.findById(this.user)
    console.log(user)
    new Message({
        receptor: user,
        body: `مشترک گرامی، درخواست شما برای سرویس ${service.name} با شماره پیگیری ${this._id.toString()} با وضعیت ${this.state} ثبت شد.`,
        name: user.fullname,
        email: user.email,

    }).save()
})

module.exports = exports = schema