const request = require('request-promise')
const apiKey= '336953437532384C596166326C346458546A4A5870513D3D'
const send = async (rcv, body)=>{
    try{
        let resp = await request.post({
            url: `https://api.kavenegar.com/v1/${apiKey}/sms/send.json`,
            formData: {
                receptor: `${rcv}`,
                message: body
            }
        })
        // console.log(resp)
    }
    catch(e){
        console.log(e);
    }
}

module.exports = exports = send;