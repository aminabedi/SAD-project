var mongoose = require('mongoose')
var Schema  = mongoose.Schema
let _ = require('underscore')
var schema = new Schema({
    firstName: {type: String},
    lastName: {type: String},
    iban: {type: String},
    natCode: {type: String, unique: true},
    phone: {type: String, required: true,  unique: true},
    email: {type: String, unique: true},
    notif: {type: String, enum: ["SMS", "EMAIL"],},
    role: { type: String, enum: ["Customer", "Employee", "Admin"], default: "Customer"},
    token: {type: String, },
    salary: {type: Number, default: 100},
    active: {type: Boolean, default: true},
    verified: {type: Boolean, default: false},
    admin: {type: Boolean, default: false}, 
    rules: mongoose.Schema.Types.Mixed,
    config: mongoose.Schema.Types.Mixed
},  {toObject: {virtuals: true}, toJSON: {virtuals: true}})

schema.virtual('fullname').get(function(){
    return `${this.firstName}-${this.lastName}`
})

schema.virtual('isAdmin').get(function(){
    return this.role === "Admin"
})

schema.virtual('isEmployee').get(function(){
    return this.role === "Admin" || this.role ==="Employee"
})


schema.pre('save', function(next){
    this.token = `${Math.random()*10000*1000}`
    if(this.notif === "EMAIL" && (!this.email ||this.email===""))
        return next(new Error('Email required if notif type is Email'))
    next()
})

schema.methods.get_balances = async function(){
    const Transaction = mongoose.model('Transaction')
    let a = await Transaction.aggregate([
        {
        $match: {destination: this._id}
    },
    {
        $group: {
            _id: "$currency",
            amount: {$sum: '$amount'}
        }
    }
    ])
    let b = await Transaction.aggregate([
        {
        $match: {source: this._id}
    },
    {
        $group: {
            _id: "$currency",
            amount: {$sum: '$amount'}
        }
    }
    ])
    // console.log("AB", a, b, _.reduce(a, (m, t)=>t.currency==="IRR"?m+t.amount:m, 0));
    return {
        usd: _.reduce(a, (m, t)=>t._id==="USD"?m+t.amount:m, 0) -  _.reduce(b, (m, t)=>t._id==="USD"?m+t.amount:m, 0),
        eur: _.reduce(a, (m, t)=>t._id==="EUR"?m+t.amount:m, 0) -  _.reduce(b, (m, t)=>t._id==="EUR"?m+t.amount:m, 0),
        irr: _.reduce(a, (m, t)=>t._id==="IRR"?m+t.amount:m, 0) -  _.reduce(b, (m, t)=>t._id==="IRR"?m+t.amount:m, 0),
    }
}

module.exports = exports = schema