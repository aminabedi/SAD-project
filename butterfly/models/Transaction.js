var mongoose = require('mongoose')
var Schema  = mongoose.Schema
var schema = new Schema({
    made_by: {type: mongoose.Schema.ObjectId, required: true, ref: 'User'},
    source: {type: mongoose.Schema.ObjectId, ref: 'User'},
    destination: {type: mongoose.Schema.ObjectId, ref: 'User'},
    request: {type: mongoose.Schema.ObjectId, ref: 'Request'},
    currency: {type: String, enum: ["IRR", "USD", "EUR"], required: true},
    amount: {type: Number, default: 0},
    action: {type: String}
},  {toObject: {virtuals: true}, toJSON: {virtuals: true}})



schema.pre('save', async function(next){
    //default action names
    if(!this.source)
        this.action = "charge"
    else if(!this.destination)
        this.action = "withdraw"
    else if(!this.action){
        this.action = "exchange"
    }


    const User = mongoose.model('User')
    const Message = mongoose.model('Message')
    let admin = await User.findOne({admin:true})
    if(this.source){
        this.source = await User.findById(this.source._id)
        this.source.balances = await this.source.get_balances()
        if( this.source.balances[this.currency]<this.amount){
            return next(new Error(`INSUFFICIENT ${this.currency} BALANCE`))
        }
    }
    if(this.source && this.source.id === admin.id){
        admin.balances = await admin.get_balances()
        if(admin.balances[this.currency.toLowerCase()]<this.amount){
            console.log("LOW BALANCE!", admin.balances[this.currency.toLowerCase()], this.amount)
            await new Message({
                user: admin,
                name: "باترفلای",
                email: "admin@butterfly.com",
                body: `Your ${this.currency} balance should be increased at least by ${this.amount}`
            }).save()
        }
    }
    next()
})

schema.post('save', async function(){
    const User = mongoose.model('User')
    const Message = mongoose.model('Message')
    let source = this.source?await User.findById(this.source).exec():null
    let destination = this.destination?await User.findById(this.destination).exec():null
    let admin = await User.findOne({admin: true})
    if(source)
        await new Message({
            receptor: source,
            user: admin,
            body: `مبلغ ${this.amount} ${this.currency} به جهت عملیات ${this.action} از حساب شما کسر شد.`,
            name: source.fullname,
            email: source.email
        }).save()
        
    if(destination)
        await new Message({
            receptor: destination,
            user: admin,
            body: `مبلغ ${this.amount} ${this.currency} از سوی ${source?source.fullname:"عملیات بانکی"} به جهت عملیات ${this.action} به حساب شما واریز شد.`,
            name: destination.fullname,
            email: destination.email
        }).save()
    
})
module.exports = exports = schema