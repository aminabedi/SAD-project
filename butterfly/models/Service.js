var mongoose = require('mongoose')
var Schema  = mongoose.Schema
var schema = new Schema({
    name: String,
    description: String,
    photo: {type: String, default: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRD48wpkZtBVblCjDMmEdzM7pjdXF_Uzmw9SqagRCHGqn9jJ3RAPg"},
    price: {type: Number, default: 0},
    currency: {type: String, enum: ["USD", "EUR", "IRR"]},
    destination: {type: mongoose.Schema.ObjectId, ref: 'User'},
    active: {type: Boolean, default: true}
},  {toObject: {virtuals: true}, toJSON: {virtuals: true}})

module.exports = exports = schema