var mongoose = require('mongoose');
var Schema  = mongoose.Schema;
const sendSMS = require('./utils/sendSMS')
var schema = new Schema({
    receptor: {type: mongoose.Schema.ObjectId,  default: mongoose.Types.ObjectId("5b26a31b9fea2a2833e312e4"), ref: 'User'}, 
    user: {type: mongoose.Schema.ObjectId, default: mongoose.Types.ObjectId("5b26a31b9fea2a2833e312e4"), ref: 'User'},
    name: {type: String},
    email: {type: String},
    body: {type: String, required: true},
    created_at: {type: Date, default: Date.now},
},  {toObject: {virtuals: true}, toJSON: {virtuals: true}});

// schema.pre('save', async function(next){
//     const User = mongoose.model('User');
//     let user = await User.findOne({admin: true}).exec()
//     if(!this.receptor){
//         this.receptor = user._id    
//     }
//     console.log("PRE SAVE")
//     if(!this.user){
//         this.user = user._id    
//     }
//     next();
// })

schema.post('save', async function(){
    try{
    const User = mongoose.model('User');
    let receptor = await User.findById(this.receptor).exec()
    let user = await User.findById(this.user).exec()
    await sendSMS(receptor.phone, `شما یک پیام از ${user.fullname} دارید: ${this.body} \n باترفلای`)
    }
    catch(e){

    }
})

module.exports = exports = schema