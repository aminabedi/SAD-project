var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
const Promise = require("bluebird");

var mongoose = require("mongoose");
mongoose.connect(
  process.env.MONGO_URI || "mongodb://localhost:27017/butterfly"
);
const User = mongoose.model("User", require("./models/User"));
mongoose.model("Request", require("./models/Request"));
mongoose.model("Service", require("./models/Service"));
mongoose.model("Transaction", require("./models/Transaction"));
mongoose.model("Message", require("./models/Message"));

const updateRatesLoop = async () => {
  const getRates = require("./utils/getRates");
  while (true) {
    try {
      const rates = await getRates();
      await User.update({ admin: true }, { rules: { IRR: 1, ...rates } }, {});
    } catch (e) {
      console.log("ERROR", e)
    }
    await Promise.delay(60 * 1000);

  }
};

const updateRequestsLoop = async () => {
  const timeOutRequests = require('./utils/timeOutRequests');
  while(true){
  try {
    await timeOutRequests();
  } catch (e) {
    console.log("ERROR", e)
  }
  await Promise.delay(60 * 1000);
}
}

const salaryLoop = async () => {
  const transferSalaries = require('./utils/transferSalaries');
  while(true){ 
  try {
    await transferSalaries();
  } catch (e) {
    console.log("ERROR", e)
  }
  await Promise.delay(60 * 1000);
}
}


updateRatesLoop();
updateRequestsLoop();
salaryLoop()

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.locals = {
  title: process.env.TITLE
};

// uncomment after placing your favicon in /public
// app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.get('/resetFactory', async (req, res)=>{
  const mongoose = require('mongoose')
  const User = mongoose.model('User')
  const Transaction = mongoose.model('Transaction')
  const Request = mongoose.model('Request')
  const Message = mongoose.model('Message')
  const Service = mongoose.model('Service')
  res.clearCookie("token");
  try{
    await User.remove({})
    await Transaction.remove({})
    await Request.remove({})
    await Message.remove({})
    await Service.remove({})
    await new User({
      "phone": "09215445325",
      "verified": true,
      "email": "aminabedi96@gmail.com",
      "iban": "IR111111111111111111111111",
      "natCode": "0019145252",
      "lastName": "admin",
      "firstName": "system",
      "active": true, 
      "token": "1234",
      "role": "Admin",
      "admin": true,
      "rules": {
        "IRR": 1,
        "USD": 42000,
        "EUR": 123480
      },
      "config": {
        "commission": "1",
        "USD": {
          "lower": "1",
          "upper": "1000"
        },
        "EUR": {
          "lower": "1",
          "upper": "1000"
        },
        "IRR": {
          "lower": "1000",
          "upper": "10000000"
        }
      }
    }).save()
    res.send("SUCCESS")
  }
  catch(e){
      console.log(e)
      res.send("ERROR "+ e)
  }
})



app.use(async (req, res, next) => {
  res.locals.req = req
  const User = require('mongoose').model('User');
  let admin = await User.findOne({admin:true})
  res.locals.config = req.config = admin.config || {commission:1, IRR: {lower: 0, upper: 1000}, USD: {lower: 0, upper: 100}, EUR: {lower: 0, upper: 10}}
  req.rules = admin.rules
  req.admin = admin
  if (!req.cookies.token) {
    res.locals.menues = [
      { label: "ورود", link: "/user/signin" },
      { label: "ثبت نام", link: "/user/signup" },
      { label: "نرخ تبدیل ارز", link: "/rates" },
      { label: "تماس‌ با ما", link: "/contact" },
      { label: "شرایط و قوانین", link: "/tos" }
    ];
    return next();
  }
  req.user = await User.findById(
    await jwt.verify(req.cookies.token, process.env.SECRET)["_id"]
  );
  if(!req.user.active){
    return res.send("YOUR ACCOUNT HAS BEEN DEACTIVATED, PLEASE CONTACT OUR SUPPORT!")
  }
  // req.user = await User.findOne(
  //   {admin: true}
  // );
  res.locals.menues = [
    { label: "پروفایل من", link: "/user/"},
    { label: "درخواست‌ها", link: "/request/list" },
    { label: "تراکنش‌ها", link: "/transaction/list" },
    { label: "شارژ", link: "/transaction/charge" },
    { label: "تسویه", link: "/transaction/withdraw" },
    { label: "تبدیل", link: "/transaction/exchange" },
    { label: "انتقال وجه", link: "/transaction/transfer" },
  ];
  if (req.user.isAdmin)
    res.locals.menues.push(
      { label: "کاربران", link: "/user/list" , hr: true},
      { label: "پیام‌ها", link: "/contact/list" },
      { label: "سرویس‌ها", link: "/service/list" },
      { label: "تنظیمات", link: "/config"}
    );
  
  res.locals.menues.push({ label: "نرخ تبدیل ارز", link: "/rates", hr:true });
  res.locals.menues.push({ label: "شرایط و قوانین", link: "/tos" });
  res.locals.menues.push({ label: "خروج", link: "/user/signout" });
  res.locals.user = req.user;
  res.locals.error = req.query.error;
  res.locals.success = req.query.success;
  next();
});

global.requireAuth = role => async (req, res, next) => {
  if (!req.cookies.token) {
    return res.redirect("/");
  }
  req.user = await User.findById(
    await jwt.verify(req.cookies.token, process.env.SECRET)["_id"]
  );
  // req.user = await User.findOne(
  //   {admin: true}
  // );
  res.locals.balances = await req.user.get_balances()
  if (role === "Admin" && !req.user.isAdmin)
    return res.render("error", { error: "Access Denied!" });
  else if (role === "Employee" && !req.user.isEmployee)
    return res.render("error", { error: "Access Denied!" });
  next();
};
var routes = require("./routes/index");
app.use("/", routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render("error", {
    message: err.message,
    error: {}
  });
});

module.exports = app;
