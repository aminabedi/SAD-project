const express = require('express')
const router = express.Router()
const Service = require('mongoose').model('Service')
const Requiest = require('mongoose').model('Request')
const passport = require('passport')
const auth = passport.authenticate('local', {loginFailure: '/login'})

/* GET users listing. */
router.get('/list', async (req, res, next)=> {
    return res.render('services', {
        services: await Service.find()
    })
})


router.get('/create', global.requireAuth('Admin'), async(req, res, next)=>{
    return res.render('service')
})

router.post('/create', global.requireAuth('Admin'), async(req, res, next)=>{
    try{
        await new Service({destination: req.user, ...req.body}).save()
        return res.redirect('/service/create?success=1')
    }catch(e){
        return res.redirect(encodeURI('/service/create?error=خطا؛ داخلی'))
    }
})

router.get('/edit/:id', global.requireAuth('Admin'), async(req, res, next)=>{
    let service = await Service.findById(req.params.id)
    res.render('service', {service})
})

router.post('/edit/:id', global.requireAuth('Admin'), async(req, res, next)=>{
    await Service.update({_id: req.params.id}, req.body, {})
    res.redirect(req.params.id)
})

router.get('/:id', async (req, res)=>{
    return res.render('service', {
        service: await Service.findById(req.params.id),
    })
})

router.post('/:id', auth, async (req, res)=>{
    try{
        await new Request({
            user: req.user,
            request: req.params.id,
        }).save()
        return res.redirect('/contact?success=1')
    }
    catch(e){
        return res.redirect(encodeURI('/contact?error=خطا؛ داخلی'))
    }
})

module.exports = router
