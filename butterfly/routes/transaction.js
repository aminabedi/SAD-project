const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Transaction = mongoose.model('Transaction')
const User = mongoose.model('User')


router.get('/list', global.requireAuth(), async (req, res, next)=>{
  return res.render('transactions', {transactions: await Transaction.find(req.user.isEmployee?{}:{$or: [{destination: req.user}, {source: req.user}]}).populate('source destination made_by request')})
})

router.get('/charge', global.requireAuth(), async(req, res)=>{
    res.render('charge', {isAdmin: req.user.isAdmin})
  })


router.post('/charge', global.requireAuth(), async(req, res)=>{
  console.log("CHARGING", req.config, req.body.amount)
  if(req.body.amount<parseInt(req.config[req.body.currency].lower) || req.body.amount>parseInt(req.config[req.body.currency].upper)){
    return res.redirect(encodeURI('/transaction/charge?error=خطا؛ سقف و کف تعیین شده رعایت نشده است'))
  }
    try{
      await new Transaction({
        made_by: req.user,
        destination: req.user,
        currency: req.user.isAdmin?req.body.currency:"IRR",
        amount: req.body.amount
      }).save()
      return res.redirect('charge?success=1')
    }catch(e){
      console.log("CHARGE ERROR", e)
      return res.redirect('charge?error=1')
    }
  })
  
  router.get('/withdraw', global.requireAuth(), async(req, res)=>{
    res.render('withdraw')
  })
  
  router.post('/withdraw', global.requireAuth(), async(req, res)=>{
    let a = req.body.amount * (100 + req.config.commission) /100;
    if(a<parseInt(req.config.IRR.lower) || a>parseInt(req.config.IRR.upper))
      return res.redirect(encodeURI('?error=خطا؛ سقف و کف تعیین شده رعایت نشده است'))
    req.user.balances = req.user.get_balances()
    if(req.user.balances.IRR<a){
      return res.redirect(encodeURI('?error=خطا؛موجودی حساب ریالی جهت تصفیه کافی نیست'))
    }
    try{
      
      await new Transaction({
        made_by: req.user,
        source: req.user,
        currency: "IRR",
        amount: req.body.amount,
        action: "withdraw"
      }).save()
      await new Transaction({
        made_by: req.user,
        source: req.user,
        destination: await User.findOne({admin: true}),
        currency: "IRR",
        amount: a - req.body.amount,
        action: "karmozd"
    }).save()
      return res.redirect('/transaction/withdraw?success=1')
    }catch(e){
      console.log(e);
      return res.redirect(encodeURI('/transaction/withdraw?error=خطا؛ موجودی کافی نیست'))
    }
  })
  
  router.get('/exchange', global.requireAuth(), async(req, res)=>{
    res.render('exchange', {
      rules: req.rules
    })
  })
  
  router.post('/exchange', global.requireAuth(), async(req, res)=>{
    try{
    req.user.balances = await req.user.get_balances()
    let rules = req.rules
    let admin = req.admin
    const sc = req.body.source_currency
    const dc = req.body.dest_currency
    
    let a = req.body.amount * (100+parseInt(req.config.commission)) /100, b = req.body.amount * 
            rules[sc] /
            rules[dc]
    console.log(a, sc, "=>>", b, dc, req.config, rules);
    if(a<parseInt(req.config[sc].lower) || a >parseInt(req.config[sc].upper)){
      return res.redirect(encodeURI('/transaction/exchange?error=خطا؛ سقف و کف تعیین شده برای ارز مبدأ رعایت نشده است'))
    }
    if(a>req.user.balances[sc])
      return res.redirect(encodeURI('/transaction/exchange?error=خطا؛عدم موجودی کافی'))
    await new Transaction({
        made_by: req.user,
        source: req.user,
        destination: admin,
        currency: sc,
        amount: req.body.amount,
        action: "exchange"
    }).save()
    await new Transaction({
      made_by: req.user,
      source: req.user,
      destination: admin,
      currency: sc,
      amount: a - req.body.amount,
      action: "karmozd"
  }).save()
    await new Transaction({
        made_by: req.user,
        source: admin,
        destination: req.user,
        currency: dc,
        amount: b,
        action: "exchange"
    }).save()
    return res.redirect('/transaction/exchange?success=1')
    }
    catch(e){
      console.log(e)
      res.redirect(encodeURI('/transaction/exchange?error=خطا؛ داخلی'))
    }
  })


  router.get('/transfer', global.requireAuth(), async(req, res)=>{
    res.render('transfer')
  })

  router.post('/transfer', global.requireAuth(), async(req, res)=>{
    try{
    req.user.balances = await req.user.get_balances()
    const User = mongoose.model('User')
    let a = req.body.amount
    if(a<parseInt(req.config[req.body.currency].lower) || a >parseInt(req.config[req.body.currency].upper)){
      return res.redirect(encodeURI('/transaction/transfer?error=خطا؛ سقف و کف تعیین شده برای ارز مورد انتقال رعایت نشده است'))
    }
    let d = await User.findOne({phone: req.body.destination})
    
    if(a>req.user.balances[req.body.currency])
      return res.redirect(encodeURI('/transaction/transfer?error=خطا؛ اعتبار شما برای انجام این تراکنش کافی نیست'))
    if(!d){
      await new User({
        phone: req.body.destination
      }).save()
      d = await User.findOne({phone: req.body.destination})
    }
    if(!d.active){
      return res.redirect(encodeURI('?error=کاربر مقصد توسط مدیریت مسدود شده است')) 
    }
    await new Transaction({
        made_by: req.user,
        source: req.user,
        destination: d,
        currency: req.body.currency,
        amount: a,
        action: "transfer"
    }).save()

    return res.redirect('/transaction/transfer?success=1')
    }
    catch(e){
      console.log(e)
      res.redirect(encodeURI('/transaction/transfer?error=خطا؛'))
    }
  })
  
module.exports = router
