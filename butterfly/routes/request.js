const express = require('express')
const router = express.Router()
const Request = require('mongoose').model('Request')
const Service = require('mongoose').model('Service')
const Transaction = require('mongoose').model('Transaction')
const User = require('mongoose').model('User')


/* GET users listing. */
router.get('/list', async (req, res, next)=> {
    try{
    let reqs = await Request.find(req.user.role==="Customer"?{user: req.user}:{}).populate('service user')
    // console.log("REQS", reqs)
    return res.render('requests', {
        requests: reqs
    });
    }catch(err){
        console.log("ERR:", err)
    }   
})

router.get('/create', async(req, res)=>{
    let services = await Service.find({active: true})
    res.render('request', {services})
})

router.get('/create/:id', global.requireAuth(), async(req, res)=>{
    req.user.balances = await req.user.get_balances()
    let service = await Service.findById(req.params.id)
    if(req.user.balances[service.currency]<service.price){
        return res.redirect(encodeURI('/request/create?error=خطا؛ عدم موجودی کافی از نوع ارز مربوطه جهت ثبت سفارش'))
    }
    await new Request({
        user: req.user, 
        service, 
        type: "Service"
    }).save()
    return res.redirect('/request/list')

})

router.get('/approve/:id',global.requireAuth('Employee'), async (req, res)=>{

    try{
        let request = await Request.findById(req.params.id)

        let service = await Service.findById(request.service)
        let source = await User.findById(request.user)
        source.balances = await source.get_balances()
        if(source.balances[service.currency]<service.price){
            await Request.update({_id: req.params.id}, {reviewed_by: req.user, state: "REJECTED"})
            return res.render('/list?success=1')
        }
        await new Transaction({
            request: request,
            source: request.user,
            made_by: req.user,
            destination: service.destination,
            currency: service.currency,
            amount: service.price,
            action: service.name
        }).save()
        await Request.update({_id: req.params.id}, {approved_by: req.user, state: "APPROVED"})
        res.redirect(`/request/list?success=1`)
    }
    catch(err){
        console.log("ERR:", err)
        res.redirect(encodeURI(`/request/list?error=خطا؛ داخلی`))
    }
})


router.get('/reject/:id',global.requireAuth('Employee'), async (req, res)=>{
    try{
        await Request.update({_id:req.params.id, state: "PENDING"}, {state: "REJECTED"}, {})
        res.redirect('../list?success=1')
    }
    catch(err){
        res.redirect(`../list?error=0`)
    }
})
module.exports = router
