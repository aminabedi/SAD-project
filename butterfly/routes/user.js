const express = require('express')
const router = express.Router()
const Promise = require('bluebird')
const { sendSMS } = require('../utils/auth')
const passport = require('passport')
var jwt = require('jsonwebtoken')

const User = require('mongoose').model('User')
const Transaction = require('mongoose').model('Transaction')

/* GET users listing. */
router.get('/signup', async (req, res, next)=> {
  return res.render('signup')
})

router.post('/signup', async (req, res)=>{
  try{
    await User.update({phone: req.body.phone, verified: false}, {token: '1234', ...req.body}, {upsert: true})
    await sendSMS(req.body.phone,'1234', {template: "butterfly"})
    res.redirect(`/user/verify?phone=${req.body.phone}`)
  }
  catch(err){
    console.log("ERR IN SIGNUP", err)
    res.send(err)
  }
})
router.get('/signin', async(req, res)=>{
  return res.render('signin')
})

router.post('/signin', async(req, res)=>{
  await User.update({phone: req.body.phone}, {token: '1234'})
  return res.redirect(`verify?phone=${req.body.phone}`)
})

router.get('/signout', async(req, res)=>{
  res.clearCookie('token');
  res.redirect('/')
})

router.get('/verify', async(req, res)=>{
  return res.render('verify', req.query)
})
router.post('/verify', async(req, res)=>{
  let {phone, token} = req.body
  let user = await User.findOne({ phone, token })
  if (!user) {  return res.redirect(encodeURI(`verify?error=خطا؛ کد نادرست&phone=${req.query.phone}`)) }
  let cookie = jwt.sign({_id: user._id}, process.env.SECRET)
  res.cookie('token', cookie , { maxAge: 9000000, httpOnly: true });
  return res.redirect('/panel')
})




router.get('/list', global.requireAuth('Admin'), async(req, res)=>{
  res.render('users', {
    users: await Promise.map(await User.find(), async(u)=>{u.balances = await u.get_balances(); return u})
  })
})

router.get('/:id?', global.requireAuth(), async(req, res)=>{
  if(!req.params.id) req.params.id = req.user._id
  res.render('profile', {user: await User.findById(req.params.id), admin: req.user.role==="Admin"})
})


router.post('/:id?', global.requireAuth(), async(req, res)=>{
  if(!req.params.id) req.params.id = req.user._id
  req.body.active = (!req.body.active || req.body.active === "true")
  console.log("UPDATING PROFILE")
  if(req.user.role!=="Admin"){
    delete req.body.role
  }
  console.log("PROF:", req.body)
  try{
    console.log("HERE")
    await User.update({_id: req.params.id}, req.body, {})
    res.redirect('?success=1')
  }
  catch(err){
    console.log(err)
    res.redirect(encodeURI(`?error=خطا؛ داخلی`))
  }
})
module.exports = router
