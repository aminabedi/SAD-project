var express = require('express');
const router = express.Router();
const user = require('./user')
const contact = require('./contact')
const service = require('./service')
const request = require('./request')
const transaction = require('./transaction')
const getRates = require('../utils/getRates')

const _ = require('underscore')
const mongoose = require('mongoose')
const User = mongoose.model('User')
const Transaction = mongoose.model('Transaction')
const Request = mongoose.model('Request')
const Message = mongoose.model('Message')
const Service = mongoose.model('Service')

/* GET home page. */
router.get('/', async(req, res)=>{
  res.render('landing')
})
router.get('/tos', (req, res)=>{
  res.render('tos')
})


router.get('/panel', async (req, res)=>{
  console.log(req.user)
  if(req.user){
    req.user.balances = await req.user.get_balances();
  }
  res.render('panel')
  // res.render('home', {})
})
router.get('/config', global.requireAuth('Admin'), async(req, res)=>{
  let admin = await User.findOne({admin:true})
  res.render('config')
})
router.post('/config', global.requireAuth('Admin'), async(req, res)=>{
  let { commission, usdl, usdu, eurl, euru, irrl, irru } = req.body
  await User.update({admin:true}, {config: {commission,
    USD: {lower: usdl, upper: usdu},
    EUR: {lower: eurl, upper: euru},
    IRR: {lower: irrl, upper: irru }
  }}, {}).exec()
  res.redirect('/config?success=1')
})

router.use('/user', user)
router.use('/contact', contact)
router.use('/service', service)


router.use('/request', request)
router.use('/transaction', transaction)

router.use('/rates', async(req, res)=>{
  const { amount, fromCurrency, toCurrency } = req.body
  const rates = await getRates()
  const rialAmount = amount * (rates[fromCurrency] || 1)
  const rawAmount = Math.trunc(rialAmount / (rates[toCurrency] || 1))
  const calculatedAmount = rawAmount * (100 - req.config.commission) / 100.0
  if (!isNaN(calculatedAmount)) {
    const bounds = req.config
    if (amount < bounds[fromCurrency].lower) {
      res.redirect(encodeURI('/rates?error=خطا؛ مقدار تعیین شده از کف تراکنش این واحد پول کمتر است'))
    }
    if (amount > bounds[fromCurrency].upper) {
      res.redirect(encodeURI('/rates?error=خطا؛ مقدار تعیین شده از سقف تراکنش این واحد پول بیشتر است'))
    }
  }
  const context = {
    rates,
    amount, fromCurrency, toCurrency, calculatedAmount
  }
  res.render('rates', context)
})

module.exports = router;
