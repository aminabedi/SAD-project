const express = require('express')
const router = express.Router()
const Message = require('mongoose').model('Message')
const passport = require('passport')
const auth = passport.authenticate('local', {loginFailure: '/login'})

/* GET users listing. */
router.get('/', async (req, res, next)=> {
  return res.render('contact')
})

router.post('/', async (req, res)=>{
  try{
    await new Message({
        name: req.body.name,
        body: req.body.body,
        email: req.body.email,
    }).save()
    res.redirect(`/contact?success=1`)
  }
  catch(err){
    console.log(err)
    res.redirect(encodeURI(`/contact?error=خطا؛ داخلی`))
  }
})

router.get('/list', global.requireAuth('Admin'), async (req, res, next)=>{
  return res.render('messages', {messages: await Message.find({})})
})


module.exports = router
