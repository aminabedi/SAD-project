from __future__ import with_statement
from fabric.api import local, cd, run, env, settings, hosts, shell_env

SOURCE_DIR = '/root/server'
GIT_REPO = 'git@gitlab.com:aminabedi/SAD-project.git'


def deploy_internal(branch, env):
    run('rm -rf "%s"' % SOURCE_DIR)
    run('git clone -b "%s" "%s" "%s" > /dev/null' %
        (branch, GIT_REPO, SOURCE_DIR))
    with cd(SOURCE_DIR + '/butterfly'), shell_env(NODE_ENV=env):
        run('docker-compose pull')
        run('docker-compose build')
        with settings(warn_only=True):
            run('docker-compose stop')
            run('docker-compose rm -v --force')
        run('docker-compose up --no-build -d')


def stop_all_containers():
    run('docker stop $(docker ps -aq)')


def remove_all_containers():
    run('docker rm $(docker ps -a -f status=exited -q)')


def remove_all_images():
    run('docker rmi $(docker images -a -q)')


@hosts('root@butterfly.havaliza.ir')
def deploy_prod():
    deploy_internal('master', 'production')


@hosts('root@butterfly.havaliza.ir')
def log_prod():
    run('docker logs -f --tail=100 butterfly_butterfly_1')


@hosts('root@butterfly.havaliza.ir')
def mongo_prod():
    env.output_prefix = False
    run('docker exec -it butterfly_mongo_1 mongo butterfly')


@hosts('root@butterfly.havaliza.ir')
def redis_prod():
    env.output_prefix = False
    run('docker exec -it butterfly_redis_1 redis-cli')


@hosts('root@butterfly.havaliza.ir')
def mongo_backup():
    env.output_prefix = False
    run('docker exec -it server_mongo_1 mongodump -o /data/db/backup/backup-`date "+%Y-%m-%d-%H-%M-%S"`')
