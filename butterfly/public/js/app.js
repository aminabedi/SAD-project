Foundation.Abide.defaults.patterns["phone"] = {
  test: function(text) {
    return (
      Foundation.Abide.defaults.patterns["number"].test(text) &&
      text.startsWith("09") &&
      text.length === 11
    );
  }
};

Foundation.Abide.defaults.patterns["auth-code"] = {
  test: function(text) {
    return (
      Foundation.Abide.defaults.patterns["number"].test(text) &&
      text.length === 4
    );
  }
};


Foundation.Abide.defaults.patterns["iban"] = {
  test: function(text) {
    console.log(text)
    console.log(text.substring(2))
    console.log(text.length, text.startsWith('IR'))
    return (
      text.length === 26 &&
      text.startsWith('IR') &&
      Foundation.Abide.defaults.patterns["number"].test(text.substring(2))
    );
  }
};


Foundation.Abide.defaults.patterns["national-code"] = {
  test: function(text) {
    return (
      Foundation.Abide.defaults.patterns["number"].test(text) &&
      text.length === 10
    );
  }
};



$(document).foundation();

$(document).ready(function() {
  $(".bf-header").click(function() {
    $(".bf-menu").toggleClass("hide-for-small-only");
  });
});
